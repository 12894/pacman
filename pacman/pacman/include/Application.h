#pragma once

#include <iostream>
#include "Object3D.h"
#include "glm/glm.hpp"
#include "GLFW/glfw3.h"
#include "packman.h"

class Application {
public:
	Application();
	~Application();

	GLFWwindow* window;
	Object3D camara;
	void keyboard(int key, int scancode, int actions, int mods);
	
	void setup();
	void update();
	void display();
	void reshape(int w, int h);
	
	void pinta();

	void pintaJuego();
	void pintaEditor();

	

	bool arriba, abajo, derecha, izquierda;
	
	
	enum Numeros {
		pildora = 0,
		pared,
		jugador,
		vacio
	};

	int mapin[18][21]
	{
		{ 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 },
		{ 1,2,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,1 },
		{ 1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1 },
		{ 1,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,1 },
		{ 1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1 },
		{ 1,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,1 },
		{ 1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1 },
		{ 1,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,1 },
		{ 1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1 },
		{ 1,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,1 },
		{ 1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1 },
		{ 1,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,1 },
		{ 1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1 },
		{ 1,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,1 },
		{ 1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1 },
		{ 1,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,1 },
		{ 1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,1 },
		{ 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 }


	};

private:
	bool flagX = false;
	bool flagY = false;
	bool flagZ = false;
	bool GameMode = false;

	packman player;
	Object3D Cuadrado;
	glm::mat4 camera;
	glm::vec3 eye, target, angles;
	glm::mat4	transform,
		transform2,
		Rotacion,
		perspectiva;

	GLuint idTransform;
	void game();
};

