
#include "Application.h"
#include <iostream>
#include <vector>

#include "GL\glew.h"
#include <gl/GL.h>
#include <gl/GLU.h>
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "ShaderFuncs.h"
#include "Object3D.h"
#include "packman.h"





std::vector<GLfloat> vertexPositions = {

	1.0f,   1.0f, 1.0f, 1.0f,//a
	-1.0f,  -1.0f, 1.0f, 1.0f,//c
	1.0f,  -1.0f, 1.0f, 1.0f,//b
	1.0f,   1.0f, 1.0f, 1.0f,//a
	-1.0f,   1.0f, 1.0f, 1.0f,//d
	-1.0f,  -1.0f, 1.0f, 1.0f,//c
							  //Cara 1
							  // -------------------- 
							  //Cara 2

	1.0f, 1.0f, -1.0f, 1.0f,//e
	-1.0f, 1.0f,  1.0f, 1.0f,//d
	1.0f, 1.0f,  1.0f, 1.0f,//a
	1.0f, 1.0f, -1.0f, 1.0f,//e
	-1.0f, 1.0f, -1.0f, 1.0f,//f
	-1.0f, 1.0f,  1.0f, 1.0f,//d
	//Cara 2
	// -------------------- 
	//Cara 3

	-1.0f, 1.0f,  1.0f, 1.0f,//d
	-1.0f,-1.0f, -1.0f, 1.0f,//g
	-1.0f,-1.0f,  1.0f, 1.0f,//c
	-1.0f, 1.0f,  1.0f, 1.0f,//d
	-1.0f, 1.0f, -1.0f, 1.0f,//f
	-1.0f,-1.0f, -1.0f, 1.0f,//g
	//Cara 3
	// -------------------- 
	//Cara 4

	1.0f, 1.0f, -1.0f, 1.0f,//e
	1.0f,-1.0f,  1.0f, 1.0f,//b
	1.0f,-1.0f, -1.0f, 1.0f,//h
	1.0f, 1.0f, -1.0f, 1.0f,//e
	1.0f, 1.0f,  1.0f, 1.0f,//a
	1.0f,-1.0f,  1.0f, 1.0f,//b
	//Cara 4
	// -------------------- 
	//Cara 5

	1.0f,-1.0f,  1.0f, 1.0f,//b
	-1.0f,-1.0f, -1.0f, 1.0f,//g
	1.0f,-1.0f, -1.0f, 1.0f,//h
	1.0f,-1.0f,  1.0f, 1.0f,//b
	-1.0f,-1.0f,  1.0f, 1.0f,//c
	-1.0f,-1.0f, -1.0f, 1.0f,//g
	//Cara 5
	// -------------------- 
	//Cara 6

	1.0f,-1.0f, -1.0f, 1.0f,//h
	-1.0f, 1.0f, -1.0f, 1.0f,//f
	1.0f, 1.0f, -1.0f, 1.0f,//e
	1.0f,-1.0f, -1.0f, 1.0f,//h
	-1.0f,-1.0f, -1.0f, 1.0f,//g
	-1.0f, 1.0f, -1.0f, 1.0f,//f
};



Application::Application() :
	target(0.0f, 0.0f, 0.0f),
	transform(glm::mat4(1.0f)),
	transform2(glm::mat4(1.0f)),
	Rotacion(glm::mat4(1.0f)),
	angles(1.0f, 0.0f, 0.0f),
	camera(glm::mat4(1.0f))
{}

Application::~Application() 
{}

void Application::update()
{



}

void Application::keyboard(int key, int scancode, int actions, int mods)
{
	if (GLFW_RELEASE == actions && GLFW_KEY_ESCAPE == key)
	{
	}
	if (GLFW_RELEASE == actions && GLFW_KEY_P == key) {
		
	/*	target = */
		camera = glm::lookAt(eye, target, glm::vec3(0, 1, 0));
		GameMode = !GameMode;
	
	}
	if (GLFW_PRESS == actions && GLFW_KEY_W == key) {
		arriba = true;
		
	}
	if (GLFW_PRESS == actions && GLFW_KEY_S == key) {
		abajo = true;
		
	}
	if (GLFW_PRESS == actions && GLFW_KEY_D == key) {
		derecha = true;

	}
	if (GLFW_PRESS == actions && GLFW_KEY_A == key) {
		izquierda = true;

	}
	if (GLFW_PRESS == actions && GLFW_KEY_O == key)
	{
		int i, j;
		std::cout << "quieres editar el mapa" << std::endl;
		std::cout << "que fila" << std::endl;
		std::cin >> i;
		std::cout << "que columna" << std::endl;
		std::cin >> j;
		std::cout << "que quieres poner muro =1 o 0 para vaciarlo" << std::endl;
		std::cin >> mapin[i][j];

	}

	
		//glfwTerminate();
		/*switch (key)
		{
		case GLFW_KEY_W:
			arriba();
			break;
		case GLFW_KEY_S:
			eye.x -= 1;
			break;
		case GLFW_KEY_A:
			eye.y -= 1;
			break;
		case GLFW_KEY_D:
			eye.y += 1;
			break;

		case GLFW_KEY_UP:
			eye.z += 1;
			break;
		case GLFW_KEY_DOWN:
			eye.z -= 1;
			break;
		case GLFW_KEY_LEFT:
			target.x -= 1;
			break;
		case GLFW_KEY_RIGHT:
			target.x += 1;
			break;

		case GLFW_KEY_O:
			target.y += 1;
			break;
		case GLFW_KEY_L:
			target.y -= 1;
			break;

		case GLFW_KEY_K:
			target.z -= 1;
			break;
		case GLFW_KEY_P:
			target.z += 1;
			break;
		}
	}*/
		
}

void Application::setup()
{
	player.setUp(mapin);
	Cuadrado.setShaders("shaders\\vertex.vs", "shaders\\fragment.fs");
	Cuadrado.setup(vertexPositions);
	
}



void Application::display()
{
	//Borramos el buffer de color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	
	if (GameMode == false) {
		pintaEditor();
	}
	else {
		pintaJuego();
	}

	/*cuano tenga cin a una funcion tu metes un numer en i j  ahi igual 0 o 1*/


}

void Application::reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
}
enum cosas
{
	pildoras = 0,
	muro,
	jugador
};
void Application::pinta()

{
	                               

}
void Application::pintaJuego()
{
	perspectiva = glm::perspective(45.0f, 1.0f, 0.8f, 1000.0f);
	eye = glm::vec3(player.x, 0.0f, player.y - 1);
	target = eye;
	target.z -= 1;
	for (int i = 0; i < 18; i++)
	{
		for (int j = 0; j < 21; j++) {
			if (mapin[i][j] == jugador) {
				if ((derecha || izquierda || arriba || abajo) && GameMode)
				{

					eye = glm::vec3((i * 2), 0, (j * 2));
					target = eye;
					if ((mapin[i][j - 1] == vacio || mapin[i][j - 1] == pildora) && derecha == true) {
						target.z -= 5;
						mapin[i][j] = vacio;
						(mapin[i][j - 1]) = jugador;
						derecha = false;
						break;
					}
					else if ((mapin[i][j + 1] == vacio || mapin[i][j + 1] == pildora) && izquierda == true) {
						target.z += 5;
						mapin[i][j] = vacio;
						(mapin[i][j + 1]) = jugador;

						izquierda = false;
						break;
					}
					else if ((mapin[i - 1][j] == vacio || mapin[i - 1][j] == pildora) && arriba == true) {
						(mapin[i][j]) = vacio;
						(mapin[i - 1][j]) = jugador;
						target.x -= 5;
						arriba = false;
						break;
					}
					else if ((mapin[i + 1][j] == vacio || mapin[i + 1][j] == pildora) && abajo == true)
					{
						(mapin[i][j]) = vacio;
						(mapin[i + 1][j]) = jugador;
						abajo = false;
						target.x += 5;
						break;
					}
					target.z -= 1;
					break;
				}
			}
		}
	}

	camera = glm::lookAt(eye, target, glm::vec3(0.0f, 1.0f, 0.0f));

	//Seleccionamos los shaders a usar

	for (int i = 0; i < 18; i++)
	{
		for (int j = 0; j < 21; j++) {
			transform = glm::translate(glm::mat4(1.0f), glm::vec3(i * 2, 0.0f, j * 2));
			transform = perspectiva *
				camera * transform;

			if (mapin[i][j] == pared)
			{
				Cuadrado.draw(transform, glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
			}
			if (mapin[i][j] == pildora)
			{
				Cuadrado.draw(glm::scale(transform, glm::vec3(.2f, .2f, .2f)), glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
			}

			if (mapin[i][j] == jugador) {
				Cuadrado.draw(glm::scale(transform, glm::vec3(.2f, .2f, .2f))/*transform*/, glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
			}
		}
	}
}
void Application::pintaEditor()
{
	perspectiva = glm::perspective(45.0f, 1.0f, 1.0f, 1000.0f);
	target = glm::vec3(21, 0, 12);
	eye = glm::vec3(10, 82, 12);
	for (int i = 0; i < 18; i++)
	{
		for (int j = 0; j < 21; j++) {
			if (mapin[i][j] == jugador) {
				if ((derecha || izquierda || arriba || abajo) && GameMode)
				{

					eye = glm::vec3((i * 2), 0, (j * 2));
					target = eye;
					if ((mapin[i][j - 1] == vacio || mapin[i][j - 1] == pildora) && derecha == true) {
						target.z -= 5;
						mapin[i][j] = vacio;
						(mapin[i][j - 1]) = jugador;
						derecha = false;
						break;
					}
					else if ((mapin[i][j + 1] == vacio || mapin[i][j + 1] == pildora) && izquierda == true) {
						target.z += 5;
						mapin[i][j] = vacio;
						(mapin[i][j + 1]) = jugador;

						izquierda = false;
						break;
					}
					else if ((mapin[i - 1][j] == vacio || mapin[i - 1][j] == pildora) && arriba == true) {
						(mapin[i][j]) = vacio;
						(mapin[i - 1][j]) = jugador;
						target.x -= 5;
						arriba = false;
						break;
					}
					else if ((mapin[i + 1][j] == vacio || mapin[i + 1][j] == pildora) && abajo == true)
					{
						(mapin[i][j]) = vacio;
						(mapin[i + 1][j]) = jugador;
						abajo = false;
						target.x += 5;
						break;
					}
					target.z -= 1;
					break;
				}
			}
		}
	}

	camera = glm::lookAt(eye, target, glm::vec3(0.0f, 1.0f, 0.0f));

	//Seleccionamos los shaders a usar

	for (int i = 0; i < 18; i++)
	{
		for (int j = 0; j < 21; j++) {
			transform = glm::translate(glm::mat4(1.0f), glm::vec3(i * 2, 0.0f, j * 2));
			transform = perspectiva *
				camera * transform;

			if (mapin[i][j] == pared)
			{
				Cuadrado.draw(transform, glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
			}
			if (mapin[i][j] == pildora)
			{
				Cuadrado.draw(glm::scale(transform, glm::vec3(.2f, .2f, .2f)), glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
			}

			if (mapin[i][j] == jugador) {
				Cuadrado.draw(glm::scale(transform, glm::vec3(.2f, .2f, .2f))/*transform*/, glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
			}
		}
	}
}
void Application::game()
{
}
//
//void Application::arriba()
//{
//	int Y= player.x-1;
//	int X = player.y;
//	
//	
//}
//
//void Application::abajo()
//{
//}
//
//void Application::izquierda()
//{
//}
//
//void Application::derecha()
//{
//}
