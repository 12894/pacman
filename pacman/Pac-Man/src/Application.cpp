#include "Application.h"
#include <iostream>
#include <vector>

#include "GL\glew.h"
#include <gl/GL.h>
#include <gl/GLU.h>
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"


#include "ShaderFuncs.h"

std::vector<GLfloat> vertexPositions = {
	//Quad1
	-1.0f, 1.0f, -1.0f, 1.0f, //h
	 1.0f, -1.0f, -1.0f, 1.0f, //n
	 1.0f, 1.0f,	-1.0f, 1.0f, //g
	-1.0f, 1.0f, -1.0f, 1.0f, //h
	 1.0f, -1.0f, -1.0f, 1.0f, //n
	-1.0f, -1.0f, -1.0f, 1.0f, //m

							   
    -1.0f, 1.0f, -1.0f, 1.0f, //h
	 1.0f, 1.0f,	-1.0f, 1.0f, //g
	-1.0f, 1.0f, 1.0f, 1.0f, //d
	 1.0f, 1.0f,	-1.0f, 1.0f, //g
	-1.0f, 1.0f, 1.0f, 1.0f, //d
	 1.0f, 1.0f, 1.0f, 1.0f, //c						   

	 -1.0f, 1.0f, 1.0f, 1.0f, //d
	 1.0f, 1.0f, 1.0f, 1.0f, //c
	 -1.0f, -1.0f, 1.0, 1.0, //a
	 1.0f, 1.0f, 1.0f, 1.0f, //c
	 -1.0f, -1.0f, 1.0, 1.0, //a
	 1.0f, -1.0f, 1.0f, 1.0f, //b												   //Quad3
													   
	 1.0f, 1.0f, 1.0f, 1.0f, //c
	 1.0f, -1.0f, 1.0f, 1.0f, //b
	 1.0f, 1.0f, -1.0f, 1.0f, //i
	 1.0f, -1.0f, 1.0f, 1.0f, //b
	 1.0f, 1.0f, -1.0f, 1.0f, //i
	 1.0f, -1.0f, -1.0f, 1.0f, //j

	 -1.0f, 1.0f, 1.0f, 1.0f, //d
	 -1.0f, -1.0f, 1.0, 1.0, //a
	 -1.0f, 1.0f, -1.0f, 1.0f, //h
	 -1.0f, -1.0f, 1.0, 1.0, //a
	 -1.0f, 1.0f, -1.0f, 1.0f, //h
	 -1.0f, -1.0f, -1.0f, 1.0f, //m
												

	 1.0f, -1.0f, 1.0f, 1.0f, //b
	 -1.0f, -1.0f, 1.0, 1.0, //a
	 -1.0f, -1.0f, -1.0f, 1.0f, //m
	 1.0f, -1.0f, 1.0f, 1.0f, //b
	 -1.0f, -1.0f, -1.0f, 1.0f, //m
	 1.0f, -1.0f, -1.0f, 1.0f, //n
																										  
};






Application::Application() : eye(0.0f, 100.0f, 100.0f),
target(0.0f, 0.0f, 0.0f),
transform(glm::mat4(1.0f)),
angles(0.0f, 0.0f, 0.0f)
{}

Application::~Application()
{}

void Application::update()
{
	

	camera = glm::lookAt(eye, target, glm::vec3(0.0f, 10.0f, 0.0f));
	transform = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f,0.0f));
	transform = glm::perspective(100.0f, 1024.0f / 768.0f, .1f, 100.0f) *
		camera * transform;
}

void Application::keyboard(int key, int scancode, int actions, int mods)
{
	if (actions == GLFW_RELEASE)
	{
		switch (key)
		{
		case GLFW_KEY_ESCAPE: glfwSetWindowShouldClose(window, GLFW_TRUE);
			break;

		case GLFW_KEY_W:
			angles.y++;
			break;

		case GLFW_KEY_S:
			angles.x += 5.0f;
			break;
		}
	}
	
}

void Application::setup()
{
	Cube.Shaders("Shaders\\Vertex.vs", "Shaders\\fragment.fs");
	Cube.setup(vertexPositions,NULL);

}


void Application::display()
{
	
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//Seleccionamos los shaders a usar


	for (int i = 0; i < 11; ++i)
	{
		for (int j = 0; j < 11;++j)
		{
			transform = glm::translate(glm::mat4(1.0f),glm::vec3(i*2,0,j*2));
			transform = glm::perspective(20.0f, 1024.0f / 768.0f, 0.1f,-100.f) *
				camera * transform;

			if (pacMap[i][j] == 1)
				Cube.draw(glm::scale(transform,glm::vec3(0.4f,0.4f,0.4f)), glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
			if (pacMap[i][j] == 2)
				Cube.draw(glm::scale(transform, glm::vec3(0.2f, 0.2f, 0.2f)), glm::vec4(1.0f, 0.0f, 1.0f, 1.0f));
		}
	}
	
}

void Application::reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
}